#include <stdio.h>

int main(void) {
	char s1[10], s2[10], s3[10];

	printf("Enter 3 words: ");

	scanf("%s %s %s", s1, s2, s3); //scanf scans for word delimited by space, tab, or \n
	//The perils of C, is that it's still pretty vulnerable to stack smashing... unlike other newer languages maybe

	//Try inputin: 1234567890123 abcdefghijklm zzz -that's 13 13 3 chars each..
	//To stack smash it, enter more than 10 chars as the 3rd word or any payload to the target.
	printf("The words are: ");
	printf("%s %s %s\n", s1, s2, s3);

	return 0;
}
