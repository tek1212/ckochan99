#include <stdio.h>

int main(void) {
	void readLine(char buffer[], int limit);

	char line[11];

	printf("Enter 5 lines of words (10 chars only  per line) :\n(I think this is overflow proof.. Try to overflow it if you can!!!)\n");

	for ( int i=0; i<5; i++) {
		readLine(line, 10);
		printf("%s\n\n", line);
	}
}

//Fuction to read line by line, scanf("%s") can't achieve this since it only capable to read word by word-- since a 'space' or 'tab' delimits it
//there is already a gets() function in the library.. but this shows how simple it is to construct it.
void readLine(char buffer[], int limit) {
	int i = 0;

	do {
		buffer[i++] = getchar(); 	//a more fit & direct approach than scanf("%c", buf[i++])
		if ( i>=limit ) {		//if index number reached the limit
			buffer[i] = '\0';	//mark the last index --> 11 here (limit + 1)
			break;
		}
		if (buffer[i-1] == '\n') {	//if prev read char is \n
			buffer[i-1] = '\0';		//replace that \n w. \0 marker
			break;
		}
	} while ( 1 );

}
