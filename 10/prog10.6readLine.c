#include <stdio.h>

int main(void) {
	void readLine(char buffer[]);

	char line[11];

	printf("Enter 5 lines of words (10 chars only  per line) :\nYou can stack smash it w. 30 lines sometimes or more..\n");

	for ( int i=0; i<5; i++) {
		readLine(line);
		printf("%s\n\n", line);
	}
}

//Fuction to read line by line, scanf("%s") can't achieve this since it only capable to read word by word-- since a 'space' or 'tab' delimits it
//there is already a gets() function in the library.. this just shows how simple it is to construct it.
void readLine(char buffer[]) {
	int i = 0;

	do {
		buffer[i++] = getchar(); 	//a more fit & direct approach than scanf("%c", buf[i++])
	} while ( buffer[i-1] != '\n' );

	buffer[i-1] = '\0'; 			//comment out this line, if you want to achieve some 'MAGIC'
}
