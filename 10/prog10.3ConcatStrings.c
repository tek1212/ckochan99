#include <stdio.h>

int main(void) {
	void concat(char str3[], const char * str1, const char * str2);

	const char * str1 = "Test ";	// many ways to initiate a char * strings
	const char str2[] = {"works."};

	char str3[30];

	concat(str3, str1, str2);

	printf("%s\n", str3);

	return 0;

}

void concat(char * str3, const char * str1, const char * str2) {
	int i, j;

	//copy the 1st string
	for ( i=0; str1[i] != '\0'; ++i ) {
		str3[i] = str1[i];
	}

	//copy the 2nd string
	for ( j=0; str2[j] != '\0'; ++j) {
		str3[i+j] = str2[j];
	}

	//finish the string
	str3[i+j] = '\0';
}
