#include <stdio.h>

int main(void) {
	char s1[10], s2[10], s3[10];

	printf("Enter 3 words: ");

	scanf("%9s %9s %9s", s1, s2, s3); //scanf scans for word delimited by space, tab, or \n
	//The 10s are still pretty useless imo, it only limits chars read, not discarding the remaining overflowing chars of the word..
	//but continuing on next chars token from the limit / NOT continuing from next word!

	//The perils of C, is that it's still pretty vulnerable to stack smashing... unlike other newer languages maybe
	//But this code seems to mitigate the stack smashing, as it limits the input read.

	//Try inputin: 1234567890123 abcdefghijklmn zzzzzzzzzzzzzzzzzzzzzzz -that's 13, 14, or more than 10 chars --each..
	//To stack smash it, enter more than 10 chars as the 3rd word or any payload to the target. (See that this code mitigates stack smashing)
	printf("The words are: ");
	printf("%s %s %s\n", s1, s2, s3);

	return 0;
}
