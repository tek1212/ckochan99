#include <stdio.h>
#include <stdbool.h>

bool isAlphabetic(const char c) {
	if ( (c>='a' && c<='z') || (c>='A' && c<='Z') || c=='\'' ||
		(c>='0' && c<='9') || c==',' || c=='.' )
		return true;
	else
		return false;
}

int countWords(const char string[]) {
	int wordcount = 0;
	bool lookinforword = true;

	for (int i = 0; string[i] != '\0'; i++) {
		if ( isAlphabetic(string[i]) ) {
			if (lookinforword) {
				++wordcount;
				lookinforword = false;
			}
		} else {
			lookinforword = true;
		}
	}

	return wordcount;
}

void readLine(char buffer[], int limit) {
	int i = 0;

	do {
		buffer[i++] = getchar(); 	//a more fit & direct approach than scanf("%c", buf[i++])
		if ( i>=limit ) {		//if index number reached the limit
			buffer[i] = '\0';	//mark the last index --> 121 here (limit + 1)
			break;
		}
		if (buffer[i-1] == '\n') {	//if prev read char is \n
			buffer[i-1] = '\0';		//replace that \n w. \0 marker
			break;
		}
	} while ( 1 );

}

int main(void) {
	const char * text1 = "Count my words...!";
	const char * text2 = "Here we go... Finally!";
	const char * text3 = "Damn it... O'Neils!";
	const char * text4 = "Stupid O'Neils can't even reach sub 10.05 secs.";

	printf("%s --- %i words.\n", text1, countWords(text1));
	printf("%s --- %i words.\n", text2, countWords(text2));
	printf("%s --- %i words.\n", text3, countWords(text3));
	printf("%s --- %i words.\n", text4, countWords(text4));

	char line[121];
	int  totalWords = 0;

	printf("\nType in your text. (Press 'Return' 2x, when you're done)\n\n");

	bool endoftext = false;

	while ( !endoftext ) {
		readLine(line, 120);

		if ( line[0] == '\0' )
			endoftext = true;
		else
			totalWords += countWords(line);
	}

	printf("There are %i words in the text typed.\n", totalWords);
}

