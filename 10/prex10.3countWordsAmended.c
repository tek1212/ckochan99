#include <stdio.h>
#include <stdbool.h>

bool isAlphabetic(const char c) {
	if ( (c>='a' && c<='z') || (c>='A' && c<='Z') || c=='\'' ||
		(c>='0' && c<='9') || c==',' || c=='.' )
		return true;
	else
		return false;
}

int countWords(const char string[]) {
	int wordcount = 0;
	bool lookinforword = true;

	for (int i = 0; string[i] != '\0'; i++) {
		if ( isAlphabetic(string[i]) ) {
			if (lookinforword) {
				++wordcount;
				lookinforword = false;
			}
		} else {
			lookinforword = true;
		}
	}

	return wordcount;
}

int main(void) {
	const char * text1 = "Count my words...!";
	const char * text2 = "Here we go... Finally!";
	const char * text3 = "Damn it... O'Neils!";
	const char * text4 = "Stupid O'Neils can't even reach sub 10.05 secs.";

	printf("%s --- %i words.\n", text1, countWords(text1));
	printf("%s --- %i words.\n", text2, countWords(text2));
	printf("%s --- %i words.\n", text3, countWords(text3));
	printf("%s --- %i words.\n", text4, countWords(text4));
}

