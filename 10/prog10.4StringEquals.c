#include <stdio.h>
#include <stdbool.h>

bool stringEquals(const char str1[], const char str2[]) {
	int i = 0;

	while( str1[i] == str2[i] && str1[i] != '\0' && str2[i] != '\0' ) {
		i++;
	}

	if (str1[i]=='\0' && str2[i]=='\0')
		return true;
	else
		return false;
}

int main(void) {
	bool stringEquals(const char * str1, const char * str2);

	char * stra = "string";		//again- many ways to initiate strings
	char * strb = {"string"};
	char strc[] = "string compare test";

	printf("\"%s\" and \"%s\" are : ", stra, strb); stringEquals(stra, strb) ? printf("EQUAL.\n") : printf("NOT EQUAL.\n"); //use of cond. statement
	printf("\"%s\" and \"%s\" are : ", stra, strc); stringEquals(stra, strc) ? printf("EQUAL.\n") : printf("NOT EQUAL.\n");
	printf("\"%s\" and \"%s\" are : ", stra, "string"); stringEquals(stra, "string") ? printf("EQUAL.\n") : printf("NOT EQUAL.\n");

	return 0;
}
