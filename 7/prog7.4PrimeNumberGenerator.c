//Prime Numbers Generator

#include <stdio.h>
#include <stdbool.h>

int main(void) {
	int suspectPrime, lastPrime, primes[150];
	bool isPrime;

	primes[0] = 2; primes[1] = 3;
	lastPrime = 2;

	for ( suspectPrime = 5; suspectPrime < 150; suspectPrime += 2) {
		isPrime = true;

		for ( int i = 1; isPrime && suspectPrime / primes[i] >= primes[i]; i++ ) {
			if ( suspectPrime % primes[i] == 0 )
				isPrime = false;
		}

		if ( isPrime ) {
			primes[lastPrime++] = suspectPrime;
		}

	}

	for ( int i=0; i<lastPrime; i++ ) {
		printf("%i ", primes[i]);
	}
	printf("\n");

	return 0;
}
