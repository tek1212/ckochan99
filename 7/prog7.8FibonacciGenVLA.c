#include <stdio.h>

int main(void) {
	int numFibs = 0;
	printf("How many Fibonacci numbers do you want (between 1-100) ? ");
	if (scanf("%i", &numFibs) != 1) {
		printf("Enter a number!!!\n");
		return 1;
	}

	if ( numFibs < 1 || numFibs > 100 ) {
		printf("%i is bad number, enter between 1 to 100\n", numFibs);
		return 1;
	}

	long long int Fibonacci[numFibs]; //if it's even 100... it'll overflow the <long long int>.. try it!
	Fibonacci[0] = 0;
	Fibonacci[1] = 1;

	for (int i = 2; i < numFibs; i++) {
		Fibonacci[i] = Fibonacci[i-2] + Fibonacci[i-1];
	}

	printf("The first %i Fibonacci numbers: ", numFibs);
	for (int j = 0; j < numFibs; j++)
		printf("%lli ", Fibonacci[j]);

	printf("\n");

	return 0;
}
