#include <stdio.h>

int main(void) {
	int Fibonacci[20]; //if it's even 50... it'll overflow the <int>.. try it!
	Fibonacci[0] = 0;
	Fibonacci[1] = 1;

	for (int i = 2; i < 20; i++) {
		Fibonacci[i] = Fibonacci[i-2] + Fibonacci[i-1];
	}

	printf("The first %i Fibonacci numbers: ", 20);
	for (int j = 0; j < 20; j++)
		printf("%i ", Fibonacci[j]);

	printf("\n");

	return 0;
}
