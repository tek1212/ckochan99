#include <stdio.h>

int main(void) {
	int		iVar = 101;
	float	fVar = 331.79;
	double	dVar = 8.43e11;
	char	cVar = 'M';

	_Bool	bVar = 3;

	printf("iVar = %i\n", iVar);
	printf("fVar = %f\n", fVar);
	printf("dVar = %e\n", dVar);
	printf("dVar = %g\n", dVar);
	printf("cVar = %c\n", cVar);

	printf("bVar = %i\n", bVar);

	return 0;
}
