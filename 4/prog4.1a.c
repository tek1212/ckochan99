#include <stdio.h>

int main(void) {
	//Use ivar = 2,500,000,000
	int				i_ivar = 2500000000;
	unsigned int	u_ivar = 2500000000;

	long int		l_ivar = 2500000000;

	long long int	ll_ivar = 2500000000;

	printf("i_ivar %%i = %i\n", i_ivar);
	printf("i_ivar %%li = %li\n", i_ivar);

	printf("\n");

	printf("u_ivar %%i = %i\n", u_ivar);
	printf("u_ivar %%u = %u\n", u_ivar);

	printf("\n");

	printf("l_ivar %%i = %i\n", l_ivar);
	printf("l_ivar %%li = %li\n", l_ivar);

	printf("\n");

	printf("ll_ivar %%i = %i\n", ll_ivar);
	printf("ll_ivar %%li = %li\n", ll_ivar);
	printf("ll_ivar %%lli = %lli\n", ll_ivar);

	printf("\n-------------------------\n");

	//Use jvar = 499,000,000,000
	int				i_jvar = 499000000000;
	unsigned int	u_jvar = 499000000000;

	long int		l_jvar = 499000000000;

	long long int	ll_jvar = 499000000000;

	printf("i_jvar %%i = %i\n", i_jvar);
	printf("i_jvar %%li = %li\n", i_jvar);

	printf("\n");

	printf("u_jvar %%i = %i\n", u_jvar);
	printf("u_jvar %%u = %u\n", u_jvar);

	printf("\n");

	printf("l_jvar %%i = %i\n", l_jvar);
	printf("l_jvar %%li = %li\n", l_jvar);

	printf("\n");

	printf("ll_jvar %%i = %i\n", ll_jvar);
	printf("ll_jvar %%li = %li\n", ll_jvar);
	printf("ll_jvar %%lli = %lli\n", ll_jvar);

	return 0;
}
