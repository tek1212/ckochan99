#include <stdio.h>

int main(void) {
	char c; int ret;

	printf("Enter the chars to test: "); //987^d

	//loop (scan & print) only when return is not EOF and char is not newline
	//while ( ((ret = scanf("%c", &c)) != EOF ) && c!='\n' ) { //doesn't work, still needs 2nd ^D
	while ( ((ret = scanf("%c", &c)) != EOF ) && c!='\n' ) { //same doesn't work..
		printf("%i %c\n", ret, c);
	}
	return 0;
}

//This prog isn't complete yet, termination using \n is done, but Ctrl-D (EOT) still a problem
