//Program to evaluate expressions of the form 'value operator value'

#include <stdio.h>

int main(void) {
	char op;
	float value1, value2;

	printf("Program to evaluate expressions of the form 'value operator value'\n");
	printf("Pls enter the expression: ");
	scanf("%f %c %f", &value1, &op, &value2);

	if ( op == '+' ) {
		printf("%.2f\n", value1 + value2);
	} else if ( op == '-' ) {
		printf("%.2f\n", value1 - value2);
	} else if ( op == '*' ) {
		printf("%.2f\n", value1 * value2);
	} else if ( op == '/' ) {
		if ( value2 == 0 )
			printf("Division by zero\n");
		else
			printf("%.2f", value1 / value2);
	} else {
		printf("Unknown operator\n");
	}

	return 0;
}
