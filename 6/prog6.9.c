#include <stdio.h>

int main(void) {
	printf("Program to evaluate expressions of the form 'value1 operator value2'\n");

	char op;
	float val1, val2;
	printf("Enter the expression: ");
	scanf("%f %c %f", &val1, &op, &val2);

	switch (op) {
		case ('+'):
			printf("%.2f\n", val1 + val2);
			break;
		case ('-'):
			printf("%.2f\n", val1 - val2);
			break;
		case ('*'):
		case ('x'):
			printf("%.2f\n", val1 * val2);
			break;
		case ('/'):
			if ( val2 == 0 )
				printf("Division by zero.\n");
			else
				printf("%.2f\n", val1 / val2);
			break;
		default:
			printf("Unknown operator.\n");
			break;
	}

	return 0;
}
