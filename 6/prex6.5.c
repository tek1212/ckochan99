//Integer Number Reverser
#include <stdio.h>

int main(void) {
	printf("Integer Number Reverser Program\n");

	int number;
	printf("Enter the number to reverse ?");
	scanf("%i", &number);

	if ( number < 0 ) {
		printf("-");
		number *= -1;
	}

	do {
		int end = number % 10;
		printf("%i", end);
		number /= 10;
	} while ( number != 0 );

	printf("\n");
	return 0;
}
