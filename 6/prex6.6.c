//Number Speller

#include <stdio.h>

int main(void) {
	char c;

	printf("Enter the number to be spelled: ");
	while ( scanf("%c", &c) != EOF && c != '\n' ) {
		switch (c) {
			case ('0'):
				printf("zero ");
				break;
			case ('1'):
				printf("one ");
				break;
			case ('2'):
				printf("two ");
				break;
			case ('3'):
				printf("three ");
				break;
			case ('4'):
				printf("four ");
				break;
			case ('5'):
				printf("five ");
				break;
			case ('6'):
				printf("six ");
				break;
			case ('7'):
				printf("seven ");
				break;
			case ('8'):
				printf("eight ");
				break;
			case ('9'):
				printf("nine ");
				break;
			default:
				printf("naN ");
				break;
		}//switch
	}//while !EOF

	printf("\n");

	return 0;
}
