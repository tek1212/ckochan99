#include <stdio.h>
#include <stdbool.h>

int main(void) {
	char c; int ret;

	printf("Enter the chars to test: "); //98^d

	do {
		ret = scanf("%c", &c);
		printf("%i %c\n", ret, c);
	} while (ret==1 && c != '\n') ;
	//Stop if EOF or '\n' is met 

	return 0;
}
