#include <stdio.h>

int main(void) {
	printf("TABLE of POWERS of TWO\n\n");
	printf("n		n to power of two\n");
	printf("---------------------------\n");

	for (int i=1; i<=10; ++i) {
		printf("%2i			%3i\n", i, i*i);
	}

	return 0;
}
