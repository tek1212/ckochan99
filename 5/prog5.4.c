#include <stdio.h>

int main(void){
	int number, triangularNum = 0;

	printf("Which triangular number do you want?");
	scanf("%i", &number);

	for (int i=1; i<=number; ++i) {
		triangularNum += i;
	}

	printf("The triangular number %i is %i\n", number, triangularNum);
	return 0;
}
