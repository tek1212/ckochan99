#include <stdio.h>

int main(void){
	int number, triangularNum = 0;
	_Bool otherRound = 0;

	do
	{
		printf("Which triangular number do you want ?");
		scanf("%i", &number);

		for (int i=1; i<=number; ++i) {//Can't initiate triangularNum here, since it'll just be a local var for the loop then...
			triangularNum += i;
		}

		printf("The triangular number %i is %i\n", number, triangularNum);
		triangularNum = 0;
		printf("Another triangular number (0 for no, other numbers for yes) ?");
		scanf("%i", &otherRound);
	} while ( otherRound != 0 );

	return 0;
}
