#include <stdio.h>

int main(void) {
	//int triangularNo = 0;

	for (int i=5; i<=50; i+=5) {
		printf("Triangular number of %i = %i\n", i, (i * (i+1)) / 2);
	}

	return 0;
}
