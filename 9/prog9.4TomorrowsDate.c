#include <stdio.h>
#include <stdbool.h>

struct date {
	int month;
	int day;
	int year;
};

bool isLeapYear(struct date d) {
	if ( (d.year % 4 == 0 && d.year % 100 != 0) || d.year % 400 == 0 )
		return true;
	else
		return false;
}

int numberOfDays(struct date d) {
	const int daysPerMonth[12] = {31,28,31,30,31,30,31,31,30,31,30,31};
	if ( d.month == 2 && isLeapYear(d) )
		return 29;
	else
		return daysPerMonth[d.month-1];
}

bool checkInvalidDate(struct date d) { //added this func to check for all invalid dates
	if ( d.month < 1 || d.month > 12 )
		return true;
	else if ( d.day < 1 || d.day > numberOfDays(d) )
		return true;
	return false;
}

struct date dateUpdate(struct date today) {
	struct date tomorrow;

	if ( today.day != numberOfDays(today) ) { //regular days
		tomorrow.day = today.day + 1;
		tomorrow.month = today.month;
		tomorrow.year = today.year;
	} else if ( today.month == 12 ) {	//end of year
		tomorrow.day = 1;
		tomorrow.month = 1;
		tomorrow.year = today.year + 1;
	} else {
		tomorrow.day = 1;
		tomorrow.month = today.month + 1;	//end of other months
		tomorrow.year = today.year;
	}

	return tomorrow;
}

int main(void) {
	struct date today, tomorrow;
	int read;

	printf("Enter today's date (dd mm yyyy): ");
	read = scanf("%i %i %i", &today.day, &today.month, &today.year);

	if (read != 3 || checkInvalidDate(today) ) {
		printf("Invalid date: Pls just stick to the format.. although segfaults r fun.. i know\n");
		return 1;
	}

	tomorrow = dateUpdate(today);

	printf("Tomorrow's date is: %i %i %i\n", tomorrow.day, tomorrow.month, tomorrow.year);
	return 0;
}
