#include <stdio.h>

int main(void) {
	int one, two, three;
	int ret;
	printf("Enter today's date (dd mm yyyy): ");
	ret = scanf("%i %i %i", &one, &two, &three);

	printf("%i -- %i -- %i -- %i\n", ret, one, two, three);
	return 0;
}
