#include <stdio.h>

struct date {
	int day;
	int month;
	int year;
};

struct time {
	int hour;
	int minute;
	int second;
};

struct event {
	struct date sdate;
	struct time stime;
};

int main(void) {
	//Array of struct containing struct
	struct event sevent[2];

	sevent[0].sdate.day = 25;
	sevent[0].sdate.month = 12;
	sevent[0].sdate.year = 2020;
	sevent[0].stime.hour = 21;
	sevent[0].stime.minute = 29;
	sevent[0].stime.second = 49;

	//sevent[1] = { {25, 12, 2020}, {21, 30, 55} }; //wrong init
	sevent[1] = (struct event) { {25, 12, 2020}, {21, 30, 55} }; //correct init

	printf("Event 1 is at %4i-%.2i-%.2i %.2i:%.2i:%.2i\n", sevent[0].sdate.year, sevent[0].sdate.month, sevent[0].sdate.day,
															sevent[0].stime.hour, sevent[0].stime.minute, sevent[0].stime.second);
	printf("Event 2 is at %4i-%.2i-%.2i %.2i:%.2i:%.2i\n", sevent[1].sdate.year, sevent[1].sdate.month, sevent[1].sdate.day,
															sevent[1].stime.hour, sevent[1].stime.minute, sevent[1].stime.second);

	return 0;
}
