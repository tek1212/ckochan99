#include <stdio.h>

struct time {
	int hour;
	int minute;
	int second;
};

void timeUpdate(struct time * t) {
	t->second++;			//this is a elegant and simple way of updating time (from Kochan)
	if ( t->second == 60 ) {	//if you dont believe me, take a quick glance.. try coding it urself 2 days ltr without cheating
		t->second = 0;
		++t->minute;

		if ( t->minute == 60 ) {
			t->minute = 0;
			++t->hour;

			if ( t->hour == 24 ) {
				t->hour = 0;
			}
		}
	}
}

int main(void) {
	struct time times[] = { {23,59,59}, {12,0,0}, {1,29,59},
			{11,59,59}, {23,29,59} };

	for ( int i=0; i<5 ; i++ ) {
		printf( "The time now is %.2i:%.2i:%.2i ...1 second ltr it's", times[i].hour, times[i].minute, times[i].second );
		timeUpdate( &times[i] );
		printf( " %.2i:%.2i:%.2i\n", times[i].hour, times[i].minute, times[i].second );
	}
}
