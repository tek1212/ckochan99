#include <stdio.h>

int main(void) {
	int a = 25, b = 37, c = 19;
	int sum = a + b - c;

	printf("The sum of %d and %d subtracted with %d is %d", a, b, c, sum);

	return 0;
}
