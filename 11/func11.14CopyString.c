void copystring (char * from, char * to) {

	while ( *from )
		*to++ = *from++;

	*to = '\0';
}
