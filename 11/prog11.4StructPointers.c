#include <stdio.h>

int main(void) {

	struct date {
		int day;
		int month;
		int year;
	};

	struct date today, * datePtr;

	datePtr = &today;

	datePtr->day = 11;
	datePtr->month = 9;
	datePtr->year = 2023;

	printf("Today will be %i-%.2i-%.2i.\n", datePtr->year, datePtr->month, datePtr->day);
	return 0;
}
