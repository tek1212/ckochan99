#include <stdio.h>

struct entry {
	int value;
	struct entry *next;
};

int main(void) {
	struct entry e1, e2, e3;
	struct entry *list_ptr = &e1;

	e1.value = 100;
	e1.next = &e2;

	e2.value = 200;
	e2.next = &e3;

	e3.value = 300;
	e3.next = (struct entry *) 0;

	while ( list_ptr != (struct entry *) 0 ) {
		printf("%i\n", list_ptr->value);
		list_ptr = list_ptr->next;
	}

	return 0;
}


