#include <stdio.h>

struct entry {
	int value;
	struct entry * next;
};

struct entry * findEntry(struct entry * list, int value) {
	while ( list != (struct entry *) 0 ) {
		if ( list->value == value )
			return list;
		else
			list = list->next;
	}

	return (struct entry *) 0;
}

int main(void) {

	struct entry e1, e2, e3;

	e1.value = 111;
	e2.value = 222;
	e3.value = 333;

	e1.next = &e2;
	e2.next = &e3;
	e3.next = (struct entry *) 0;

	printf("Find 100:\n");
	struct entry * result = findEntry(&e1, 100);
	result ? printf("Found %i\n", result->value) : printf("Not found\n");

	printf("Find 333:\n");
	result = findEntry(&e1, 333);
	result ? printf("Found %i\n", result->value) : printf("Not found\n");

	return 0;
}
