#include <stdio.h>

int arraySum(int * ar_ptr, const int n) {
	int sum = 0;
	int * const arrayend = ar_ptr + n;

	for ( ; ar_ptr < arrayend; ar_ptr++ )
		sum += *ar_ptr;

	return sum;
}

int main(void) {
	int array[10] = {1,2,3,4,5,6,7,8,9,10};
	printf("Triangular sum of 10 is %i\n", arraySum(array, 10) );

	return 0;
}
