#include <stdio.h>

int main(void) {
	int i1 = 333;
	int i2 = 444;

	int * const d = &i1;
	d = &i2; // not valid

	const int * e = &i2;
	*e = 555; // not valid

	printf("*d = %i\n", *d);
	printf("*e = %i\n", *e);

	return 0;
}
