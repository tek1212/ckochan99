#include <stdio.h>

int main(void) {

	struct intPtrs {
		int * p1;
		int * p2;
	};

	int i1 = 45;
	int i2 = 67;

	struct intPtrs ptr;
	ptr.p1 = &i1;
	ptr.p2 = &i2;

	printf("i1 = %i, *ptr.p1 = %i\n", i1, *(ptr.p1) );
	printf("i2 = %i, *ptr.p2 = %i\n", i2, *(ptr.p2) );

	printf("*ptr.p2 = 99\n");
	*ptr.p2 = 99;

	printf("i1 = %i, *ptr.p1 = %i\n", i1, *(ptr.p1) );
	printf("i2 = %i, *ptr.p2 = %i\n", i2, *(ptr.p2) );

	return 0;
}
