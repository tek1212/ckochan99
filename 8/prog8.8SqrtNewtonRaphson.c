#include <stdio.h>

float abslt(float x) {
	if ( x < 0 )
		return -x;
	return x;
}

float sqrtNewton(float x) {
	const float epsilon = 0.0001;
	float guess = 1.0;

	while ( abslt(x - (guess*guess)) >= epsilon )
		guess =  ( x/guess + guess ) / 2.0;

	return guess;
}

int main (void) {
	printf("Sqrt: Enter the number? ");

	float val;
	if ( scanf("%f", &val) != 1 ) {
		printf("Not a valid number\n");
		return 1;
	}

	if ( val < 0 ) {
		printf("Negative sqrt not valid here\n");
		return -1.0;
	}

	printf("Sqrt of %g = %g\n", val, sqrtNewton(val) );

	return 0;
}
