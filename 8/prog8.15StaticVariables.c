#include <stdio.h>

void auto_static(void) {
	int autovar = 1;
	static int staticvar = 1;

	printf("automatic = %i, static = %i\n", autovar++, staticvar++);
}

int main(void) {
	for (int i=0; i<5; i++) 
		auto_static();

	return 0;
}
