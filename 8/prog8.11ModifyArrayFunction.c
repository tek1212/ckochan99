#include <stdio.h>

void doubleArray(float array[], int n) {
	for (int i=0; i<n; i++)
		array[i] *= 2;
}

void printArray(float array[], int n) {
	printf("\n");
	for (int i=0; i<n; i++)
		printf("%g ", array[i]);
	printf("\n");
}

int inputArray(float array[], int n) {
	printf("Pls input %i numbers\n", n);
	for (int i=0; i<n; i++) {
		if ( scanf("%f", &array[i]) != 1 )
			return 1; //return error
	}
	return 0;
}

int main(void) {
	int num = 0;
	printf("How many elements in the array? ");

	if (scanf("%i", &num) != 1) {
		printf("\nBad input\n");
		return 1;
	}

	float farray[num];

	if	(inputArray(farray, num) != 0) {
		printf("\nPls input valid float numbers\n");
		return 1;
	}
	printArray(farray, num);
	doubleArray(farray, num);
	printArray(farray, num);

	return 0;
}
