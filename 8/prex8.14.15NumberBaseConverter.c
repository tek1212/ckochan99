#include <stdio.h>

//Global Variables
long long int oriNumber;
int base;
int digit = 0;
int convertedNumber[64];

int getNumberandBase(void) {
	printf("Enter the number to convert: ");
	if (scanf("%lli", &oriNumber) != 1)
		return 1;

	printf("Enter the base to convert into (between 2 to 16): ");
	while ( (scanf("%i", &base) != 1) || base < 2 || base > 16 ) {
		printf("Enter the base to convert into (between 2 to 16): ");
	}

	/*int ret = 0; //works similarly...
	do {
		printf("Enter the base to convert into (between 2 to 16): ");
		ret = scanf("%i", &base);
	} while ( ret != 1 || base < 2 || base > 16 ); */
		//try inputin a-z non-int if u dare
	return 0;
}

void convertNumber(void) {
	digit = 0;
	do {
		convertedNumber[digit++] = oriNumber % base; //only + numbers work for this method, you should see why..
		oriNumber /= base;
	} while ( oriNumber != 0 );
}

void displayConvertedNumber(void) {
	const char baseDigits[16] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

	printf("Converted number: ");

	for (--digit; digit >= 0; digit--)
		printf("%c", baseDigits[convertedNumber[digit]]); //only positive no works...

	printf("\n");
}

int main(void) {
	if (getNumberandBase() != 0)
		return 1;

	convertNumber();
	displayConvertedNumber();

	return 0;
}
