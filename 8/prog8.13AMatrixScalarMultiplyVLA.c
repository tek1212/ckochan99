#include <stdio.h>

int main(void) {
	void displayMatrix(int rows, int cols, int matrix[rows][cols]); //functions hv to be declared 1st b4 used/called.. compiler warning if not
	void scalarMultiply(int rows, int cols, int matrix[rows][cols], int scalar); //in multidim matrix.. the only the rows dim can be left out, the cols and so on, OR VLA hv to be declared it length

	int mtrx[3][5] =
		{
			{ 7, 16, 55, 44, 33 },
			{ 1,  2,  3, 44, 11 },
			{ 101, 22, 13, 5, 17 }
		};

	printf("Original matrix:\n");
	displayMatrix(3, 5, mtrx);

	scalarMultiply(3, 5, mtrx, 2);
	printf("Multiplied by 2:\n");
	displayMatrix(3, 5, mtrx);

	return 0;
}

void displayMatrix(int rows, int cols, int matrix[rows][cols]) { //rows & cols hv to be declared 1st, b4 being used/declared by the VLA variables.. compiler error if not...
	for (int i=0; i<rows; i++) {
		for (int j=0; j<cols; j++)
			printf("%5i", matrix[i][j]);
		printf("\n");
	}
	printf("\n");
}

void scalarMultiply(int rows, int cols, int matrix[rows][cols], int scalar) {
	for (int i=0; i<rows; i++)
		for (int j=0; j<cols; j++)
			matrix[i][j] *= scalar;
}

